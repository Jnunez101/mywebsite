---
description: "A test"
tags: []
title: "Presentations"
---
List of presentations I gave and to which club/class. They can all mostly be found [here](https://gitlab.com/Jnunez101/jnunez-presentations). Below is a more split up explanation. 

- Computer Networks 
  - A talk explaining the layers of the internet and computer networking given to the Linux Users Group. [Source](https://gitlab.com/Jnunez101/jnunez-presentations/-/tree/main/laptop-slides/lug-network-talk)
- Graphics in Linux
  - A talk on the history of graphics in Linux as well as how it is handled today. It was made in conjunction with other members of the Linux Users Group given to the Linux Users Group. [Source](https://gitlab.com/Jnunez101/jnunez-presentations/-/tree/main/laptop-slides/lug-graphics-talk-master)
- Gaming on Linux
  - A talk describing the status of Gaming on Linux using Proton and winetricks. It was given to the Linux Users Group. [Source](https://gitlab.com/Jnunez101/jnunez-presentations/-/tree/main/laptop-slides/gaming-on-linux-lug-presentation)
- Video Game Console Security
  - A talk describing the security mechanics in many video game consoles. It was given to the cyber security club, the Linux Users Group, and my Info Security Class. [Source](https://gitlab.com/Jnunez101/jnunez-presentations/-/tree/main/laptop-slides/console-security-oresec-talk)
- What is Tor?
  - A talk on the Tor browser and it's functionality and security. It was given to the Linux Users Group. [Source](https://gitlab.com/Jnunez101/jnunez-presentations/-/blob/main/laptop-slides/oresec-tor-presentation-master.tar.gz)
- Lug Distro Talk
  - An introduction Talk on linux distros given on the second Linux Users Group meeting of the fall 2021 semester. [Source](https://gitlab.com/Jnunez101/jnunez-presentations/-/tree/main/laptop-slides/lug-distros-talk) 