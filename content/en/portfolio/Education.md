---
description: "A test"
tags: []
title: "Education"
---

- MS in Computer Science January 2022 - Present
  - Colorado School of Mines
  - Notable Classes
    - Advanced High Performance Computing
    - Applied Algorithms & Data Structures
- BS in Computer Science August 2018 - December 2021
  - Colorado School of Mines
    - Focus area in Computer Engineering 
    - Notable Classes
      - Compilers
      - Computer Simulations
      - Theory of Cryptography
      - Computer Networks
      - Information Security
      - Machine Learning
      - Digital Logic