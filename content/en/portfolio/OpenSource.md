---
description: "I did an Open Source!"
tags: []
title: "Open Source Projects and Commits"
---

- [Mobile Insight](https://github.com/mobile-insight/mobileinsight-core/pull/116)
    - Expanded 5G support for newer modems via manually examining the binary produced by modems

- [MLocate](https://gitlab.com/ColoradoSchoolOfMines/mlocate)
  - Hackathon project for HackCU 2019
    - Finds the location of a device by using 3 WiFi routers
    - Displays predicted device location on a map of the Hackathon venue
    - Won 2nd place

- [Field Session Project](https://github.com/cpainterwakefield/fs-team-assembler)
  - CS@Mines field session project
    - Field session is a mandatory class for all CS students where they are placed in a project they complete over the first month and a half of summer vacation
    - Created the website that is used for Field Session team creation.
      - Personally developed the backend in NodeJS and user authentication using JSON Web Tokens and the School's authentication system, Shibboleth.

- [Hass Website](https://gitlab.com/Jnunez101/hasswebsite)
  - HASS 404 website I made on Gitlab pages
    - HTML hosted on gitlab pages at hass.jnunez.dev
- [TapVRap](https://github.com/jnunez101/TapVRap)
  - HackCU 2020
  - Using the Tap Strap device create a device that can track hand movement in 3 Dimensions.
  - The device was undeveloped at time of creation and we ended up working towards getting raw data off of the device instead
- [C-Tapp](https://github.com/jnunez101/C-Tapp)
  - A web client based replacement to the iclicker device that is 50$ at the Mines Book store
  - Hack UTD 2019

