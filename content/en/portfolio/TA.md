---
description: "A test"
tags: []
title: "Course TA"
---

- Operating Systems CSCI 442 Senior Level
  - Lead TA Spring 2022
    - Lead TA incharge of other TAs and running Projects
    - Held office hours
  - TA Spring 2021 - Fall 2021
    - Create Autograders for the class for Gradescope
      - Written in Python, and Bash
    - Held Office Hours
- Intro To Python Lab CSCI 102
  - Course TA Fall 2019
    - Answered student questions during Lab